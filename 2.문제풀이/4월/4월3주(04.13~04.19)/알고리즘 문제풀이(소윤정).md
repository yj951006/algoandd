### 수박수박수
```python
def solution(n): 
    str1 = ('수박'*n)[:n]
    # str '수박'을 n회만큼 만들고
    # ('수박'*n)[시작 번호:끝 번호]에서 시작 번호를 생략하면 
    # 문자열의 처음부터 끝 번호까지 뽑아낸다.
    return str1;
```


### 문자열 다루기 기본
```python
def solution(s):
    if (len(s) == 4 or len(s) == 6) and s.isdigit() :
        answer = True;
    else : 
        answer = False;
    return answer
```