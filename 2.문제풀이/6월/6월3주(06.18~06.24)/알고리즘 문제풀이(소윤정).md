### 1. 없는숫자 더하기
```python
def solution(numbers):
    
    # arr = list(range(10));
    arr = 0 
    for i in range(10) :
        if i not in numbers :
            arr += i;
    answer = arr
    return answer
```

### 2. 핸드폰번호가리기
```python
def solution(phone_number):
    answer = [];
    
    plength = len(phone_number);

    for i in range(plength-4):
        answer += '*'
        
    answer += phone_number[-4:]
    
    return ''.join(answer);
```