[알앤디]

# 실적인증방법1 (러닝플랫폼)
- 러닝플랫폼 3강 청취 및 댓글 달기
  3월 4주차 : http://swpsso.posco.net/idms/U61/jsp/redirect.jsp?redir_url=https%3A%2F%2Flp.posco.co.kr%2FS22%2FS22A10%2Fsso%2Fauth.do%3FredirectUrl%3DL21lbWJlci9wbGF5bGlzdC9wbGF5bGlzdFZpZXcuZG8%2FY2hubmxTZXE9Mjk2NjczJnBseWxzdFNlcT0yMzM1

[추천 강의]
1. 파이썬 활용하기
2. 파이썬 기초

# 실적인증방법2 (유투브, 강의 등 인증中 택1)
- 파이썬 강의 청취 후 정리 하여 회신 (일자별, 어떤 강의를 들었는지 내용 정리) [★추천★]
- 파이썬 강의 청취 후 감상문 작성 후 회신 (일자별 감상 내용 정리)

[추천 강의]
1. 프로그래머스 파이썬 기초 강의 
- https://programmers.co.kr/learn/courses/2#curriculum
2. 유투브 오늘코드todaycode
- https://www.youtube.com/results?search_query=%EC%98%A4%EB%8A%98%EC%BD%94%EB%93%9C+todaycode
3. 조코딩 JoCoding
- https://www.youtube.com/watch?v=3R6vFdb7YI4


[개념정리]
1. 개념정리 -> 알고리즘 -> 주차별 정리
1. 개념정리 -> 데이터베이스 -> 주차별 정리

[문제풀이]
2. 문제풀이 -> 주차별 정리

[git clone]
- 현재 보고있는 README는 main branch 입니다.
- 소스코드 및 자료는 master branch에서 관리되고있습니다.
- clone 받으신 뒤 아래의 명령어로 전환해주세요.
`git checkout master`
